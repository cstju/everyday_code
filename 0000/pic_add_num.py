# -*- coding: utf-8 -*-
# @Author: 3dlabuser
# @Date:   2016-09-02 10:25:41
# @Last Modified by:   mac@lab538
# @Last Modified time: 2016-09-02 14:55:00

from PIL import Image, ImageFilter, ImageFont, ImageDraw
import pdb

def add_num(text,img_path):
    img = Image.open(img_path)
    draw = ImageDraw.Draw(img)
    myfont = ImageFont.truetype('DejaVuSans.ttf', size=20)#设置添加文字的字体和大小
    fillcolor = (255,33,33)#字体颜色
    width, height = img.size
    #这里的fill字体颜色可以是RGB三个数字，也可以是#FF。。，还可以直接写red，green，yellow等
    draw.text((width-100, height-100), unicode(text,'utf-8'), font=myfont, fill=fillcolor)

    img.show()
    #img.save('result.png','png')

if __name__ == '__main__':
    image_path = 'luffy.png'#打开图片
    text = 'csγβ∑123@#'#这里不能输入中文,数字英文符号都可以
    add_num(text,image_path)
