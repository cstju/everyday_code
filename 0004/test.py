# -*- coding: utf-8 -*-
# @Author: 3dlabuser
# @Date:   2016-09-04 17:13:14
# @Last Modified by:   mac@lab538
# @Last Modified time: 2016-09-04 17:48:30
import re
from collections import Counter

text = 'how are u? umberella $you$ u! u. U. U@ U# u '
"""
\b	匹配一个单词边界，也就是指单词和空格间的位置。
例如， 'er\b' 可以匹配"never" 中的 'er'，但不能匹配 "verb" 中的 'er'。

[...]	用来表示一组字符,
单独列出：[amk] 匹配 'a'，'m'或'k'
"""
print 'the first ouput:  ', re.sub (r'\u|\U', 'you', text) 
print 'the second ouput: ', re.sub (r'[uU]', 'you', text) 
print 'the third ouput:  ', re.sub(r'\b[uU]', 'you', text)
print 'the fourth ouput: ', re.sub(r'[uU]\b', 'you', text)
print 'the fifth ouput:  ',re.sub(r'\b[uU]\b', 'you', text)


lis=['red', 'blue', 'red', 'green', 'blue', 'blue']
c = Counter(lis)
print Counter(lis)
print c['red']
"""
sum(c.values())  # 所有计数的总数
c.clear()  # 重置Counter对象，注意不是删除
list(c)  # 将c中的键转为列表
set(c)  # 将c中的键转为set
dict(c)  # 将c中的键值对转为字典
c.items()  # 转为(elem, cnt)格式的列表
Counter(dict(list_of_pairs))  # 从(elem, cnt)格式的列表转换为Counter类对象
c.most_common()[:-n:-1]  # 取出计数最少的n个元素
c += Counter()  # 移除0和负值
"""