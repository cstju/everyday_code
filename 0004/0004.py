# -*- coding: utf-8 -*-
# @Author: 3dlabuser
# @Date:   2016-09-04 09:09:32
# @Last Modified by:   mac@lab538
# @Last Modified time: 2016-09-04 17:45:04
from collections import Counter
import re


def creat_list(filename):
    datalist = []
    f=open(filename, 'r') 
    for line in f:
        """
        re.sub(pattern, repl, string, max=0)
        pattern：匹配模式，也就是从文本中查找到哪些符号
        repl：用repl进行替代
        max:最大匹配次数，默认为0代表全部匹配
        """
        #a| b  :匹配a或b
        #在本文本中有符号, .  ( )还有Bei@@@@jing中的@，采用以下匹配方法能将其剔除
        #content = re.sub(r"\,|\.|\n|\r|\@", "", line)
        content = re.sub(r"[,.@]", "", line)
        datalist.extend(content.strip().split(' '))
    print datalist
    return datalist


def wc(filename):
	cou = Counter(creat_list(filename))
	print  Counter(creat_list(filename))
	print cou
    

if __name__ == "__main__":
    filename = 'input.txt'
    wc(filename)