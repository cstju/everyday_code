# -*- coding: utf-8 -*-
# @Author: 3dlabuser
# @Date:   2016-09-03 19:54:53
# @Last Modified by:   mac@lab538
# @Last Modified time: 2016-09-03 23:13:57
import string
import random
import MySQLdb

def coupon_creator(num,length):
    coupon_list=[]
    for count in range(num):
    	coupon = ''
        for word in range(length):
        #string.ascii_uppercase + string.digits
        #构成包含26个大写字母和10个数字的集合
        #每次用random.choice函数随机从中挑选一个，加到字符串类型的coupon上
            coupon += random.choice(string.ascii_uppercase + string.digits)
        if coupon not in coupon_list:
            coupon_list.append(coupon)
    return coupon_list

def save_2_mysql(coupon_list):
    db = MySQLdb.connect(host='localhost', 
    	                 user='root', 
    	                 passwd='123456', 
    	                 db='everyday_code_python',#数据库名
    	                 port=3306)#端口号，可以空缺
    cur = db.cursor()
    """
    sql_create_database = 'create database if not exists activecode_db' #创建名为activecode_db的数据集
    cur.execute(sql_create_database)

    conn.select_db("activecode_db")#选中数据集
    """
    sql_create_table = 'create table if not exists test0002(id int(4) primary key,coupon char(100))'
    cur.execute(sql_create_table)
    print "创建表成功"


    for i in range(len(coupon_list)):
    	coupon = coupon_list[i]
    	sql = "insert into test0002(id,coupon) values('%i','%s')" % (i+1,coupon) 
    	print i+1,coupon
    	cur.execute(sql)
    print "写入数据成功"

    """

    cursor.execute(sql)

    sql_create_Table = 'create database if not exists activecode_db'#这一句实际上是SQL语句
    cur.execute(sql_create_database)# 使用execute方法执行SQL语句

    db.select_db("activecode_db")
    sql_create_table = 'create table if not exists test0002(test0002 char(100))'
    cur.execute(sql_create_table)

    cur.executemany('insert into test0002 values(%s)', coupon_list)
    """
    db.commit()
    cur.close()
    db.close()

if __name__ == '__main__':
    result_list = coupon_creator(200,20)
    save_2_mysql(result_list)