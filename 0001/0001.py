# -*- coding: utf-8 -*-
# @Author: 3dlabuser
# @Date:   2016-09-03 10:35:19
# @Last Modified by:   mac@lab538
# @Last Modified time: 2016-09-03 11:21:03
import string
import random
def coupon_creator(digit):
    coupon=''
    for word in range(digit):
    #string.ascii_uppercase + string.digits
    #构成包含26个大写字母和10个数字的集合
    #每次用random.choice函数随机从中挑选一个，加到字符串类型的coupon上
        coupon+=random.choice(string.ascii_uppercase + string.digits)
    return coupon
    
def two_hundred_coupons():
    data=''
    for count in range(200):
        digit=12#激活码的位数是12
        count = count+1#
        data+='coupon no.'+str(count)+'  '+coupon_creator(digit)+'\r\n'
    return data

if __name__ == '__main__':
    coupondata=open('coupondata.txt','w')
    coupondata.write(two_hundred_coupons())
    coupondata.close()